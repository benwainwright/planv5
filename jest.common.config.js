const commonInBothProjects = {
  moduleFileExtensions: ["ts", "tsx", "js"],
  preset: "ts-jest",
  setupFiles: ["jest-plugin-unhandled-promise/setup"],
  moduleNameMapper: {
    "\\.(css|less)$": "identity-obj-proxy"
  },
  reporters: [
    "default",
    [
      "jest-junit",
      {
        classNameTemplate: "{classname}",
        titleTemplate: "{title}"
      }
    ],
    "jest-allure"
  ],
  collectCoverageFrom: ["<rootDir>/packages/**/src/**/*.{ts,tsx}"]
};

module.exports = {
  projects: [
    {
      ...commonInBothProjects,
      displayName: "node",
      testEnvironment: "node",
      testRegex: [".*\\.spec\\.tsx?"]
    },
    {
      ...commonInBothProjects,
      displayName: "dom",
      testEnvironment: "jsdom",
      testRegex: [".*\\.spec\\.dom\\.tsx?"]
    }
  ]
};
