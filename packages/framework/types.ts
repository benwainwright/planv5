import { AppWebsocketUrl } from "./src/WebsocketClient";
export const FRAMEWORK_TYPES = {
  Db: Symbol.for("Db"),
  ResponseAuthHeader: Symbol.for("ResponseAuthHeader"),
  AuthorisingDispatcher: Symbol.for("AuthorisingDispatcher"),
  JwtPublicKey: Symbol.for("JwtPublicKey"),
  JwtPrivateKey: Symbol.for("JwtPrivateKey"),
  ClientStorage: Symbol.for("ClientStorage"),
  HttpRequest: Symbol.for("HttpRequest"),
  WebsocketClient: Symbol.for("WebsocketClient"),
  AppWebsocketUrl
};
