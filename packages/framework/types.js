"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WebsocketClient_1 = require("./src/WebsocketClient");
exports.FRAMEWORK_TYPES = {
    Db: Symbol.for("Db"),
    ResponseAuthHeader: Symbol.for("ResponseAuthHeader"),
    JwtPublicKey: Symbol.for("JwtPublicKey"),
    JwtPrivateKey: Symbol.for("JwtPrivateKey"),
    ClientStorage: Symbol.for("ClientStorage"),
    HttpRequest: Symbol.for("HttpRequest"),
    AppWebsocketUrl: WebsocketClient_1.AppWebsocketUrl
};
//# sourceMappingURL=types.js.map