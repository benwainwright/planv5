export declare const FRAMEWORK_TYPES: {
    Db: symbol;
    ResponseAuthHeader: symbol;
    JwtPublicKey: symbol;
    JwtPrivateKey: symbol;
    ClientStorage: symbol;
    HttpRequest: symbol;
    AppWebsocketUrl: symbol;
};
//# sourceMappingURL=types.d.ts.map