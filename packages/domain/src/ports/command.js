"use strict";
exports.__esModule = true;
var Command = /** @class */ (function () {
    function Command() {
        this.handled = false;
    }
    Command.prototype.shouldContinueHandling = function () {
        return !this.handled;
    };
    Command.prototype.markHandlingComplete = function () {
        this.handled = true;
    };
    Command.prototype.markHandlingIncomplete = function () {
        this.handled = false;
    };
    return Command;
}());
exports.Command = Command;
