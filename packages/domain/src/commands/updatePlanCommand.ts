import { Command } from "../ports/command";

/**
 * Update the details of an existing plan
 */
export class UpdatePlanCommand extends Command {
  private readonly slug: string;
  private readonly title: string;
  private readonly description: string;
  private readonly hoursPerWeek: number;

  public constructor();
  public constructor(
    slug: string,
    title: string,
    description: string,
    hoursPerWeek: number
  );
  public constructor(
    slug?: string,
    title?: string,
    description?: string,
    hoursPerWeek?: number
  ) {
    super();
    this.slug = slug || "";
    this.title = title || "";
    this.description = description || "";
    this.hoursPerWeek = hoursPerWeek || 0;
  }

  public identifier(): string {
    return "UpdatePlanCommand";
  }

  public getSlug(): string {
    return this.slug;
  }

  public getTitle(): string {
    return this.title;
  }

  public getDescription(): string {
    return this.description;
  }

  public getHoursPerWeek(): number {
    return this.hoursPerWeek;
  }
}
