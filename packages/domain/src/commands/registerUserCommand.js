"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var command_1 = require("../ports/command");
/**
 * Register a new user
 */
var RegisterUserCommand = /** @class */ (function (_super) {
    __extends(RegisterUserCommand, _super);
    function RegisterUserCommand(name, email, password) {
        var _this = _super.call(this) || this;
        _this.name = name || "";
        _this.email = email || "";
        _this.password = password || "";
        return _this;
    }
    RegisterUserCommand.prototype.getEmail = function () {
        return this.email;
    };
    RegisterUserCommand.prototype.getName = function () {
        return this.name;
    };
    RegisterUserCommand.prototype.getPassword = function () {
        return this.password;
    };
    RegisterUserCommand.prototype.identifier = function () {
        return "RegisterUserCommand";
    };
    RegisterUserCommand.prototype.toString = function () {
        return this.identifier() + "(name: " + this.name + ", email: " + this.email + ", password: " + this.password + ")";
    };
    return RegisterUserCommand;
}(command_1.Command));
exports.RegisterUserCommand = RegisterUserCommand;
