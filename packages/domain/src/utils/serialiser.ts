import "reflect-metadata";
import { serializeError } from "serialize-error";
import { injectable, inject } from "inversify";
import { Serializable } from "../ports/serializable";

export const SerialisableConstructors = Symbol("serialisableConstructors");

@injectable()
export class Serialiser {
  private readonly constructors: {};
  public constructor(
    @inject(SerialisableConstructors)
    constructors: {}
  ) {
    this.constructors = constructors;
    this.classifyObject = this.classifyObject.bind(this);
    this.objectifyClass = this.objectifyClass.bind(this);
  }

  private objectifyClass(thing: any): {} {
    let instance: any = {};
    for (const key in thing) {
      if (thing.hasOwnProperty(key)) {
        const property = thing[key] as any;
        if (Array.isArray(property)) {
          instance[key] = property.map(this.objectifyClass);
        } else if (typeof property === "object") {
          instance[key] = this.objectifyClass(property);
        } else {
          instance[key] = property;
        }
      }
    }
    if (thing instanceof Error) {
      const { stack, name, ...errorInstance } = serializeError(thing);
      instance = { ...instance, ...errorInstance };
    }
    return {
      $: typeof thing.identifier === "function" ? thing.identifier() : "object",
      instance
    };
  }

  public serialise<T extends Serializable>(thing: T): string {
    const returnObject = this.objectifyClass(thing);
    return JSON.stringify(returnObject);
  }

  private classifyObject(thing: any): any {
    if (typeof thing !== "object") {
      return thing;
    } else if (Array.isArray(thing)) {
      return thing.map(this.classifyObject);
    }

    const type = thing["$"];
    const baseThing = this.constructors.hasOwnProperty(type)
      ? new (this.constructors as any)[type]()
      : {};
    const instanceObject: any = {};
    for (const key in thing.instance) {
      instanceObject[key] = this.classifyObject(thing.instance[key]);
    }
    return Object.assign(baseThing, instanceObject);
  }

  public unSerialise<T extends Serializable>(json: string): T {
    try {
      let obj: any;
      if (typeof json === "string") {
        obj = JSON.parse(json);
      }

      if (typeof obj === "string") {
        obj = JSON.parse(obj);
      }
      return this.classifyObject(obj || json);
    } catch (error) {
      throw new Error(`Failed to parse json '${json}: ${error}'`);
    }
  }
}
