export const APP_TYPES = {
  CurrentLoginSession: Symbol.for("CurrentLoginSession"),
  EventEmitterWrapper: Symbol.for("EventEmitterWrapper"),
  UserRepository: Symbol.for("UserRepository"),
  SlugGenerator: Symbol.for("SlugGenerator"),
  PlanRepository: Symbol.for("PlanRepository"),
  LoginProvider: Symbol.for("LoginProvider"),
  Logger: Symbol.for("Logger"),
  Dispatch: Symbol.for("Dispatch"),
  LoginSessionDestroyer: Symbol.for("LoginSessionDestroyer")
};
