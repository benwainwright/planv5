const { createFsFromVolume, Volume } = require("memfs");

let fs: any = undefined;

export const reset = () => {
  fs = createFsFromVolume(new Volume());
};

export const mock = () => {
  if (fs === undefined) {
    reset();
  }
  return fs;
};
