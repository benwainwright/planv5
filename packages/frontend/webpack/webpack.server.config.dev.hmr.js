const merge = require("webpack-merge");
const { CheckerPlugin } = require("awesome-typescript-loader");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const nodeExternals = require("webpack-node-externals");

const serverConfig = {
  mode: "development",
  entry: "./src/server/run.ts",
  target: "node",
  node: {
    dns: "mock",
    net: "mock",
    __dirname: false
  },
  output: {
    filename: "server.js"
  },
  optimization: {
    minimize: false,
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false
  },
  devtool: "cheap-module-eval-source-map",
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader"
          },
          {
            loader: "ts-loader",
            options: {
              transpileOnly: true,
              experimentalWatchApi: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: ["css-loader"]
      }
    ]
  },
  plugins: [new CheckerPlugin(), new ForkTsCheckerWebpackPlugin()],
  externals: [nodeExternals({ modulesFromFile: true, whitelist: [/^@planv5/] })]
};
module.exports = merge(require("./webpack.common.config"), serverConfig);
