// 'wait-on' doesn't work for me because of this: https://github.com/jeffbski/wait-on/issues/30

const axios = require("axios");

const url = process.argv[2];
console.log(`Polling ${url}`);
setInterval(() => {
  axios
    .get(url)
    .then(response => {
      if (response.status === 200) {
        console.log("200 status code received!");
        process.exit();
      } else {
        console.log("Not ready...");
      }
    })
    .catch(() => {
      console.log("Not ready...");
    });
}, 1000);
