module.exports = environment => {
  if (environment === "prod") {
    console.log;
    return [
      require(`./webpack/webpack.client.config.prod.js`),
      require(`./webpack/webpack.server.config.prod.js`)
    ];
  } else {
    return require(`./webpack/webpack.server.config.dev.js`);
  }
};
