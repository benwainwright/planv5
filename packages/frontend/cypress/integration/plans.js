const TEST_USERNAME = "benjamin";
const TEST_USERNAME_2 = "benamin2";

describe("The plans form", () => {
  it("Redirects to the homepage if you aren't logged in", () => {
    cy.visit("/app/plans");
    cy.location("pathname").should("eq", "/app");
  });

  describe("The create button", () => {
    it("Creates an empty plan card when", () => {
      cy.register(TEST_USERNAME);
      cy.get("header").should("contain", "Plans");
      cy.visit("/app/plans");
      cy.get(".create-button").click();
      cy.get(".plan-list").find(".card");
    });
  });

  describe("The card save button", () => {
    it("Results in cards persisting between page loads", () => {
      cy.login(TEST_USERNAME);
      cy.get("header").should("contain", "Plans");
      cy.visit("/app/plans");
      cy.get(".create-button").click();

      cy.get(".card")
        .find(".title")
        .type("Foo");

      cy.get(".card")
        .find(".description")
        .type("Bar");

      cy.get(".card")
        .find("button.save")
        .click();

      cy.visit("/app/plans");

      cy.get(".card")
        .find(".title")
        .should("have.value", "Foo");

      cy.get(".card")
        .find(".description")
        .should("have.value", "Bar");

      cy.get(".create-button").click();

      cy.get(".card")
        .first()
        .find(".title")
        .type("Foo2");

      cy.get(".card")
        .first()
        .find(".description")
        .type("Bar2");

      cy.get(".card")
        .first()
        .find("button.save")
        .click();

      cy.get(".card").should("have.length", 2);
    });

    it("Persists a different set of plans when logged in as a different user", () => {
      cy.register(TEST_USERNAME_2);
      cy.get("header").should("contain", "Plans");
      cy.visit("/app/plans");
      cy.get(".card").should("not.exist");

      cy.get(".create-button").click();

      cy.get(".card")
        .find(".title")
        .type("Fish");

      cy.get(".card")
        .find(".description")
        .type("Swim");

      cy.get(".card")
        .find("button.save")
        .click();

      cy.visit("/app/plans");

      cy.get(".card")
        .find(".title")
        .should("have.value", "Fish");

      cy.get(".card")
        .find(".description")
        .should("have.value", "Swim");

      cy.get(".card").should("have.length", 1);
    });
  });
});
