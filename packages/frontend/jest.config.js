const common = require("../../jest.common.config");

module.exports = {
  ...common,
  modulePathIgnorePatterns: ["<rootDir>/dist"]
};
