module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current"
        },
        corejs: "3",
        useBuiltIns: "entry"
      }
    ],
    "@babel/typescript",
    "@babel/react"
  ],
  plugins: [
    [
      "@babel/proposal-decorators",
      {
        legacy: true
      }
    ],
    "@babel/proposal-class-properties",
    "@babel/proposal-object-rest-spread"
  ]
};
