import React, { useState, ReactNode } from "react";
import "./PlanCard.css";
import { Plan } from "@planv5/domain/entities";

interface PlanProps {
  plan?: Plan;
  user: string;
  onSave: (plan: Plan) => void;
}

const PlanCard: React.FC<PlanProps> = ({ plan, onSave, user }) => {
  const [currentPlan, setCurrentPlan] = useState<Plan | undefined>(plan);
  const [dirty, setDirty] = useState<boolean>(false);

  const onChangeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target) {
      setCurrentPlan(
        new Plan(
          user,
          currentPlan ? currentPlan.getSlug() : "",
          event.target.value,
          currentPlan ? currentPlan.getDescription() : "",
          currentPlan ? currentPlan.getHoursPerWeek() : 0,
          currentPlan ? currentPlan.getDeadlines() : undefined
        )
      );
      setDirty(true);
    }
  };

  const onChangeDescription = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    if (event.target) {
      setCurrentPlan(
        new Plan(
          user,
          currentPlan ? currentPlan.getSlug() : "",
          currentPlan ? currentPlan.getTitle() : "",
          event.target.value,
          currentPlan ? currentPlan.getHoursPerWeek() : 0,
          currentPlan ? currentPlan.getDeadlines() : undefined
        )
      );
      setDirty(true);
    }
  };

  const onSaveClick = () => {
    setDirty(false);
    if (currentPlan) {
      onSave(currentPlan);
    }
  };

  const buttons: ReactNode[] = dirty
    ? [
        <button className="save" key="save-button" onClick={onSaveClick}>
          Save
        </button>
      ]
    : [];

  return (
    <section className="card">
      <input
        className="title"
        placeholder="Enter title"
        value={currentPlan ? currentPlan.getTitle() : ""}
        onChange={onChangeTitle}
      />
      <textarea
        placeholder="enter description"
        className="description"
        onChange={onChangeDescription}
        value={currentPlan ? currentPlan.getDescription() : ""}
      />
      {buttons}
    </section>
  );
};

export default PlanCard;
