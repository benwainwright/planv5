import * as React from "react";
import { ReactElement } from "react";

export const Home = (): ReactElement => <h1>The Homepage!</h1>;
