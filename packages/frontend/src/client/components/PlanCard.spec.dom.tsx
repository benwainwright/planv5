import React from "react";
import Adapter from "enzyme-adapter-react-16";
import PlanCard from "./PlanCard";
import { act } from "react-dom/test-utils";
import { shallow, configure } from "enzyme";
import { Plan } from "@planv5/domain/entities";

configure({ adapter: new Adapter() });

describe("The plancard", () => {
  it("Doesn't show a save button when clean", () => {
    const change = jest.fn();
    const card = shallow(
      <PlanCard user="foo" plan={undefined} onSave={change} />
    );

    expect(card.find("button.save").exists()).toBeFalsy();
  });

  it("Shows a save button when you change the title", () => {
    const change = jest.fn();
    const card = shallow(
      <PlanCard user="foo" plan={undefined} onSave={change} />
    );

    act(() => {
      card.find(".title").simulate("change", { target: { value: "foo" } });
    });

    card.update();

    expect(card.find("button.save").exists()).toBeTruthy();
  });

  it("Shows a save button when you change the description", () => {
    const change = jest.fn();
    const card = shallow(
      <PlanCard user="foo" plan={undefined} onSave={change} />
    );

    act(() => {
      card
        .find(".description")
        .simulate("change", { target: { value: "foo" } });
    });

    card.update();
    expect(card.find("button.save").exists()).toBeTruthy();
  });

  it("Hides the save button when you click save", () => {
    const change = jest.fn();
    const card = shallow(
      <PlanCard user="foo" plan={undefined} onSave={change} />
    );

    act(() => {
      card
        .find(".description")
        .simulate("change", { target: { value: "foo" } });
    });

    card.update();

    act(() => {
      card.find("button.save").simulate("click");
    });

    card.update();
    expect(card.find("button.save").exists()).toBeFalsy();
  });

  it("Calls the onSave callback with a new plan when the save button is clicked", () => {
    const change = jest.fn();
    const card = shallow(
      <PlanCard user="foo" plan={undefined} onSave={change} />
    );

    act(() => {
      card.find(".title").simulate("change", { target: { value: "footitle" } });

      card
        .find(".description")
        .simulate("change", { target: { value: "foodescription" } });
    });

    card.update();

    act(() => {
      card.find("button.save").simulate("click");
    });

    card.update();

    const plan = new Plan(
      "foo",
      "",
      "footitle",
      "foodescription",
      0,
      undefined
    );

    expect(change).toHaveBeenCalledWith(plan);
  });
});
