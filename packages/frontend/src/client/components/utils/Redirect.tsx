import React from "react";
import { ServerResponse } from "http";
import { Redirect as ReactRouterRedirect, RedirectProps } from "react-router";
import { useOptionalDependency } from "./InversifyProvider";
import { canUseDom } from "../../utils/canUseDOM";

export const Redirect: React.FC<RedirectProps> = props => {
  const response = useOptionalDependency<ServerResponse>(ServerResponse);
  if (canUseDom) {
    return <ReactRouterRedirect {...props} />;
  } else if (response) {
    response.writeHead(302, { Location: props.to.toString() });
  }
  return null;
};
