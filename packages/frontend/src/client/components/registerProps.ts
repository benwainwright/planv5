export interface RegisterProps {
  onRegister: (event: React.FormEvent<HTMLFormElement>) => void;
}
