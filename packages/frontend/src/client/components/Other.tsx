import * as React from "react";
import { ReactElement } from "react";

export const Other = (): ReactElement => <h1>Elsewhere...</h1>;
