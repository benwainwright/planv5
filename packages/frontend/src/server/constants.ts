export const PRODUCTION_MODE_STRING = "production";

export const DEFAULT_SERVER_PORT = 80;

export const WEBSOCKET_PATH = "ws://localhost/data";
