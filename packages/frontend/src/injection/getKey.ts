export const getKey = (keyEnvVar: string, defaultKey: string): string => {
  if (process.env[keyEnvVar]) {
    return process.env[keyEnvVar] as string;
  } else {
    if (process.env.NODE_ENV === "production") {
      throw new Error(`Must specify ${keyEnvVar}`);
    }
    return defaultKey;
  }
};
