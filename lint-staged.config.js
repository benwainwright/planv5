module.exports = {
  "*.{ts,tsx,js}": [
    "eslint --fix",
    "git add",
    "jest --silent --bail --findRelatedTests"
  ]
};
