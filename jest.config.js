const common = require("./jest.common.config");

module.exports = {
  modulePathIgnorePatterns: ["<rootDir>/packages/.*/dist/*"],
  moduleNameMapper: {
    "@planv5/(.*)$": "<rootDir>/packages/$1",
    "\\.(css|less)$": "identity-obj-proxy"
  },
  ...common
};
