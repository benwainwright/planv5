# Architectural Patterns

This project is a MERN (Mongo, React, Express, Node) application that attempts
to stay as [SOLID](https://en.wikipedia.org/wiki/SOLID) as possible. To do this,
I've made use of the following architectural approaches:

## Hexagonal Architecture

Following an approach similar to the one described [here](https://fideloper.com/hexagonal-architecture),
I've separated the application into the following layers:

- Domain
- Application
- Framework
- Frontend

layers can only depend on other layers that are higher up in this list. So the
application layer may depend on the domain, but may not depend on the Framework

## Dependency Injection

In order to facilitate the strict separation of layers described above, each
layer defines "port" interfaces that can be implemented by the outer layers.
I've used an IOC container called [InversifyJS](https://github.com/inversify/InversifyJS)
to identify and satisfy these dependencies when they arise.

## Server/Client abstracted Command Pattern

As described in the above linked article, the frontend communicates with the
application by making use of "commands" defined in the Domain layer. The
command bus has been designed in such a way that commands can be satisfied on
either the Server or the Client. Commands will be automatically dispatched to the
backend api if client side handling does not result in the command being marked
as handled. All handlers will be automatically configured on either frontend or backend
or both depending on where framework dependencies are available.

## Event-Carried State Transfer

In order for frontend to be aware of the state of the application, the application uses a global "EventEmitter" object that is injected into any consumer that wants it. When the application performs any action that updates state, it broadcasts an event to indicate that the action is happening containing the new state. This allows the frontend/view parts of the application to update themselves without explicitly having to continually _ask_ the application what the new state is.
